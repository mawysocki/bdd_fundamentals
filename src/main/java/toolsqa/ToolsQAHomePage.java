package toolsqa;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ToolsQAHomePage {
    WebDriver webDriver;
    WebDriverWait wait;
    final String URL = "https://demoqa.com/";
    public ToolsQAHomePage(WebDriver webDriver, WebDriverWait wait) {
        this.webDriver = webDriver;
        this.wait = wait;
    }

    public void openPage() {
        webDriver.get(URL);
        wait.until(ExpectedConditions.urlToBe(URL));
    }

    public ElementsPage openElements() {
        String xpath = "//h5[text() = 'Elements']";
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath)));
        webDriver.findElement(By.xpath(xpath)).click();
        return new ElementsPage(webDriver, wait);
    }


    public AlertsPage openAlerts() {
        String xpath = "//h5[text() = 'Alerts, Frame & Windows']";
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath)));
        webDriver.findElement(By.xpath(xpath)).click();
        return new AlertsPage(webDriver, wait);
    }
}
