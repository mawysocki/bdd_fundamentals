package toolsqa;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ElementsPage {
    WebDriver webDriver;
    WebDriverWait wait;
    public ElementsPage(WebDriver webDriver, WebDriverWait wait) {
        this.webDriver = webDriver;
        this.wait = wait;
    }

    public void expand() {
        String xpath = "//span[text() = 'Check Box']";
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath)));
        webDriver.findElement(By.xpath(xpath)).click();
    }

    public void expandTree() {
        String css = "div.react-checkbox-tree ol button";
        webDriver.findElement(By.cssSelector(css)).click();

    }

    public void selectCheckBox() {
        String xpath = "//span[text()='Documents']//..//span[@class='rct-checkbox']";
        webDriver.findElement(By.xpath(xpath)).click();
    }

    public void verifyText() {
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.id("result")));
        String result = webDriver.findElement(By.id("result")).getText();
        String expectedText = "You have selected :\n" +
                "documents\n" +
                "workspace\n" +
                "react\n" +
                "angular\n" +
                "veu\n" +
                "office\n" +
                "public\n" +
                "private\n" +
                "classified\n" +
                "general";
        Assert.assertEquals(expectedText, result);
    }


}
