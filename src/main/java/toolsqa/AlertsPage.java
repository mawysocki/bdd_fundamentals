package toolsqa;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AlertsPage {
    WebDriver webDriver;
    WebDriverWait wait;
    public AlertsPage(WebDriver webDriver, WebDriverWait wait) {
        this.webDriver = webDriver;
        this.wait = wait;
    }

    public void expand() {
        String xpath = "//div[@class='header-text']//..//div[text() = 'Alerts, Frame & Windows']";
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath)));
        webDriver.findElement(By.xpath(xpath)).click();
    }

    public void selectAlerts() {
        String xpath = "//span[text()='Alerts']";
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath)));
        webDriver.findElement(By.xpath(xpath)).click();
    }

    public void clickTimeAlert() {
        String id = "timerAlertButton";
        wait.until(ExpectedConditions.elementToBeClickable(By.id(id)));
        webDriver.findElement(By.id(id)).click();
    }

    public void verifyAlert() {
        try {
            Thread.sleep(6000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        Alert alert = webDriver.switchTo().alert();
        System.out.println(alert.getText());
    }



}
