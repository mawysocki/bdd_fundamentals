package io.cucumber.skeleton.herokuapp;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;


public class LoginPage {

    private WebDriver webDriver;
    private WebDriverWait wait;

    public void goTo() {
        webDriver.get("https://the-internet.herokuapp.com/login");
    }

    public LoginPage(WebDriver webDriver, WebDriverWait wait) {
        this.webDriver = webDriver;
        this.wait = wait;
    }

    public void enterLogin(String login) {
        //Ten same element można znaleźć na 4 różne sposoby
//        webDriver.findElement(By.cssSelector("input[name='username']")).sendKeys(login);
//        webDriver.findElement(By.cssSelector("input[id='username']")).sendKeys(login);
//        webDriver.findElement(By.cssSelector("input#username")).sendKeys(login);
        webDriver.findElement(By.id("username")).sendKeys(login);
    }

    public void enterPassword(String password) {
//        webDriver.findElement(By.cssSelector("input[name='password']")).sendKeys(password);
//        webDriver.findElement(By.cssSelector("input[id='password']")).sendKeys(password);
//        webDriver.findElement(By.cssSelector("input#password")).sendKeys(password);
        webDriver.findElement(By.id("password")).sendKeys(password);
    }

    public void clickLoginButton() {
        webDriver.findElement(By.cssSelector("button[type=submit]")).click();
    }
}