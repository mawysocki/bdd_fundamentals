package io.cucumber.skeleton.herokuapp;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginVerificationPage {
    WebDriver webDriver;
    WebDriverWait wait;

    private final String messageSuccessful = "You logged into a secure area!";
    private final String messageInvalidLogin = "Your password is invalid!";

    public LoginVerificationPage(WebDriver webDriver, WebDriverWait wait) {
        this.webDriver = webDriver;
        this.wait = wait;
    }

    public void userShouldBeLoggedIn() {
        wait.until(ExpectedConditions.urlContains("/secure"));
        wait.until(ExpectedConditions.urlToBe("https://the-internet.herokuapp.com/secure"));
        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("a[href='/logout']")));
        verifyMessage(messageSuccessful);
    }

    public void userShouldNotBeLoggedIn() {
        verifyMessage(messageInvalidLogin);

    }

    private void verifyMessage(String message) {
        String actualMessage = webDriver.findElement(By.id("flash")).getText();
        Assert.assertTrue(actualMessage.contains(message));
    }
}
