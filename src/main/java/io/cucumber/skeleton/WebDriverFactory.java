package io.cucumber.skeleton;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

//Klasa dedykowana do konfiguracji webdrivera w zależności od przeglądarki jaką chcemy użyć
public class WebDriverFactory {

    //Ustawia ścieżkę do webdrivera w zależności z którego chcemy korzystać
    public static void setupSystemProperty(Browsers browser) {
        String driverPath = String.format("C:\\Webdrivers\\%s.exe", browser.driverName);
        System.setProperty(browser.propertyName, driverPath);
    }

    //Zwraca instację Webdriver w oparciu o wybraną przeglądarkę za pomocą switch z javy 14
    public static WebDriver getWebDriver(Browsers browser){
        return switch (browser) {
            case CHROME -> new ChromeDriver();
            case FIREFOX -> new FirefoxDriver();
            case EDGE -> new EdgeDriver();
        };
    }
}
