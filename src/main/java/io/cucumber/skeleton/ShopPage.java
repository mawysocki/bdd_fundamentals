package io.cucumber.skeleton;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.HashMap;
import java.util.List;
import java.util.Random;

public class ShopPage {

    WebDriver webDriver;
    WebDriverWait wait;
    final String url = "https://react-shopping-cart-67954.firebaseapp.com/";



    double currentTotalPrice = 0.0;


    public ShopPage(WebDriver webDriver, WebDriverWait wait) {
        this.webDriver = webDriver;
        this.wait = wait;
        this.webDriver.manage().window().maximize();

    }

    public void openShopWebsite() {
        webDriver.get(url);
        verifyShirtsOnPage(16);
    }

    public void verifyShirtsOnPage(int expected) {
        wait.until(ExpectedConditions.numberOfElementsToBe(By.cssSelector(ShopSelectors.itemSelector), expected));
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector(ShopSelectors.itemSelector)));
        int result = getTotalCount();

        Assert.assertEquals(expected, result);
    }

    public void selectSize(String size) {
        String xpath = String.format("//span[text() = '%s']", size);
        WebElement element = webDriver.findElement(By.xpath(xpath));
        element.click();
    }

    private int getTotalCount() {
        return webDriver.findElements(By.cssSelector(ShopSelectors.itemSelector)).size();
    }

    public int getExpectedCountForSize(String size) {
        //Wykorzystanie mapy do połączenia rozmiaru i liczby koszulek dla danego rozmiaru
        HashMap<String, Integer> countForSize = new HashMap<>();
        countForSize.put("XS", 1);
        countForSize.put("S", 2);
        countForSize.put("M", 1);
        countForSize.put("ML", 2);
        countForSize.put("L", 10);
        countForSize.put("XL", 10);
        countForSize.put("XXL", 11);
        return countForSize.get(size);
    }

    public void selectItem(int index) {
        webDriver.findElements(By.cssSelector(ShopSelectors.addToCartButtonsSelector)).get(index - 1).click();
    }

    public void selectItem(String name) {
        String selector = String.format(ShopSelectors.selectItemByNameSelector, name);
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(selector)));
        webDriver.findElement(By.xpath(selector)).click();
    }

    public void selectRandomItem() {
        Random r = new Random();
        int index = r.nextInt(getTotalCount());
        List<WebElement> elements = webDriver.findElements(By.cssSelector(ShopSelectors.itemSelector));
        String price = elements.get(index).findElement(By.cssSelector("p.sc-124al1g-6")).getText();
        double realPrice = extractPrice(price);
        currentTotalPrice += realPrice;
        elements.get(index).findElement(By.cssSelector("button")).click();
    }

    public void closeCart() {
        By selector = By.cssSelector(ShopSelectors.closeCartSelector);
        wait.until(ExpectedConditions.elementToBeClickable(selector));
        webDriver.findElement(selector).click();
        wait.until(ExpectedConditions.attributeToBe(By.cssSelector(ShopSelectors.openCartSelector), "title", "Products in cart quantity"));
    }

    public void verifyTotalPrice(double expectedTotalPrice) {
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector(ShopSelectors.totalPriceSelector)));
        String price = webDriver.findElement(By.cssSelector(ShopSelectors.totalPriceSelector)).getText();
        double realPrice = extractPrice(price, true);
        Assert.assertEquals(realPrice, expectedTotalPrice, 0.0);
    }

    //Metoda bezargumentowa wykorzystuje pola z klasy do sprawdzenia ceny całkowitej
    public void verifyTotalPrice() {
        verifyTotalPrice(currentTotalPrice);
    }

    private double extractPrice(String price, boolean isSpace) {
        StringBuilder unwantedMark = new StringBuilder("$");
        if (isSpace) {
            unwantedMark.append(" ");
        }
        price = price.replace(unwantedMark.toString(), "");
        return Double.parseDouble(price);
    }

    //Przeciążenie metody extractPrice
    //Jeśli chcemy wydobyć cenę bez spacji to nie musimy podawać w argumencie isSpace=false.
    //Użycie jednoargumentowej metody spowoduje wywołanie jej z wartością isSpace=false
    private double extractPrice(String price) {
        return extractPrice(price, false);
    }


}
