package io.cucumber.skeleton;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

//Typ ENUM pozwala mieć obiekty tylko takie jakie tu są wylistowane
//To powoduje, że odpytując o przeglądarkę mamy jedynie do wyboru 3 wymienione przez nas.
//Dzięki temu nie da się wprowadzić błędnej wartości
public enum Browsers {

    //Obiekt tworzy się wewnątrz za pomocą wylistowania obiektów i podaniu argumentów dla konstruktora
    //Nie wymaga słowa kluczowego "NEW"
    CHROME("webdriver.chrome.driver", "chromedriver"),
    FIREFOX("webdriver.gecko.driver", "geckodriver"),
    EDGE("webdriver.edge.driver", "msedgedriver");

    //Obiekty typu Browser posiadają po 2 pole które są zdefiniowane już przy tworzeniu obiektu
    //Są finalne, wiec mogą być publiczne, bo nie grozi im modyfikacja
    public final String propertyName;
    public final String driverName;

    //Klasyczny konstruktor z dwoma parametrami
    Browsers(String propeName, String driverName) {
        this.propertyName = propeName;
        this.driverName = driverName;
    }
}
