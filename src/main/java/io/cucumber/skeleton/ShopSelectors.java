package io.cucumber.skeleton;

public class ShopSelectors {

    //Dedykowana klasa do przechowywania selektorów
    public static final String itemSelector = "main div.sc-124al1g-2";
    public static final String selectItemByNameSelector = "//p[text() = '%s']//..//button";
    public static final String addToCartButtonsSelector = "button.sc-124al1g-0";
    public static final String closeCartSelector = "button.sc-1h98xa9-0";
    public static final String openCartSelector = "div.sc-1h98xa9-3";
    public static final String totalPriceSelector = "p.sc-1h98xa9-9";




}
