Feature: Shopping
  #UWAGA: Strona przesta�a dzia�a� :(
  https://react-shopping-cart-67954.firebaseapp.com/

  #Test tak prymitywny, �e nie ma �adnych krok�w (When)
  Scenario: Visit shop
    Given  I go to shop website
#    Then  I see 16 t-shirts in offer - krok ten z parametrem 16 zosta� w��czony do kroku wchodzenia na stron�

  #Pozwala uruchamia� ten sam scenariusz na kilka sposob�w
  #Jeden wiersz Examples to jedno uruchomienie testu.
  #Scenariusz jest ten sam, tylko zmieniaja si� dane wej�ciowe
  Scenario Outline: Filters
    Given   I go to shop website
    When  I select "<size>" size
    Then  I see <count> t-shirts in offer

    Examples:
      | size | count |
      | XL   | 10    |
      | S    | 2     |

  Scenario: Select Item
    Given   I go to shop website
    When    I click 3 Item
    And     I close cart
    When    I click 4 Item
#    And     I click last Item - krok ten nie istnieje ale mo�na go sobie samemu zaimplementowa�
    Then    Verify if total price is 55.35

  Scenario: Do full shopping
    Given   I go to shop website
    When    I select "S" size
    And     I select "Blue Sweatshirt" t-shirt
    And     I close cart
    And     I select "S" size
    And     I select "ML" size
    And     I select "Black Tule Oversized" t-shirt
    Then    Verify if total price is 51.95

  Scenario: Buy random t-shirts
    Given  I go to shop website
    And    I see 16 t-shirts in offer
    When   I select random t-shirt
    And    I close cart
    And    I select random t-shirt
    And    I close cart
    When   I select random t-shirt
    Then   Verify total price