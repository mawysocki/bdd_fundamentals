Feature: Biedronka

  Scenario: Visit main page
    Given I open browser
    And   I go Biedronka home
    Then  I see Biedronka logo

  Scenario: Select category
    Given I open browser
    And   I go Biedronka home
    Then  I see Biedronka logo
    And   I accept cookies
    When  I select "electronics" category
    Then  I see page with "electronics" category


  Scenario: Select filters
    Given I open browser
    And   I go Biedronka home
    And  I see Biedronka logo
    And   I accept cookies
    And  I select "electronics" category
    And  I see page with "electronics" category
    When I expand price filter
    And  I select the cheapest filter
    Then Verify if items are in range 10 to 50

  Scenario Outline: Many Filters
    Given I open browser
    And   I go Biedronka home
    And  I see Biedronka logo
    And   I accept cookies
    And  I select "electronics" category
    And  I see page with "electronics" category
    When I expand price filter
    And  I select filter <index>
    Then Verify if items are in range <min> to <max>
    Examples:
      | index | min | max  |
      | 1     | 10  | 50   |
      | 2     | 50  | 100  |
      | 3     | 100 | 9999 |

  Scenario: Visit main page2
    Given I open browser
    And   I go Biedronka home
    And   I see Biedronka logo
    When  I accept cookies
    When  I select "electronics" category
    Then  I see page with "electronics" category
    When  I add to cart 1 element