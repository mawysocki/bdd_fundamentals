Feature: Login

  Scenario: Visit secret website
    When I enter login "tomsmith" and password "SuperSecretPassword!"
    And  I click enter button
    Then I see that the user is logged in

  Scenario: Check wrong password
    When I enter login "tomsmith" and password "IncorrectPassword"
    And  I click enter button
    Then I see that the user is not logged in

  #Scenario Outline prezentuje obydwa scenariusze przedstawione powy�ej
  #1 - u�ycie prawid�owego has�a
  #2 - u�ycie b��dnego has�a
  #3 - w obu przypadkach scenariusz jest ten sam ale s� inne dane wej�ciowe oraz inny efekt ko�cowy
  #Przy u�ycie examples mo�na sparametryzowa� nie tylko proste warto�ci typu login czy has�o ale tak�e wykonywane kroki jak expected result
  Scenario Outline: Check password
    When I enter login "<login>" and password "<password>"
    And  I click enter button
    Then <ExpectedResult>


    Examples:
      | login    | password             | ExpectedResult                       |
      | tomsmith | SuperSecretPassword! | I see that the user is logged in     |
      | tomsmith | IncorrectPassword    | I see that the user is not logged in |