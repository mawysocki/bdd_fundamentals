package junit;

import io.cucumber.skeleton.Browsers;
import io.cucumber.skeleton.WebDriverFactory;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import javax.security.auth.callback.CallbackHandler;
import java.time.Instant;

public class BiedronkaTest extends AbstractTest{
    public final String URL = "https://home.biedronka.pl/kuchnia-1/";

    @Test
    public void test1() {
        webDriver.get(URL);
        wait.until(ExpectedConditions.urlToBe(URL));
        String cookieID = "onetrust-accept-btn-handler";
        wait.until(ExpectedConditions.elementToBeClickable(By.id(cookieID)));
        webDriver.findElement(By.id(cookieID)).click();
        webDriver.findElement(By.xpath("//span[contains(text(),'Marka')]")).click();
        String marka = "DAFI";
        String xpath = String.format("//label[contains(text(),'%s')]//..//..//a", marka);
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath)));
        String text = webDriver.findElement(By.xpath(String.format("//label[contains(text(),'%s')]",marka))).getText();
        text = text.replace(marka,"").replace("(","").replace(")","").trim();
        int expectedCount = Integer.parseInt(text);
        System.out.println("Text: " + text);
        webDriver.findElement(By.xpath(xpath)).click();
        wait.until(ExpectedConditions.urlToBe(String.format("%s?prefn1=brand&prefv1=%s", URL, marka)));
        int counter = webDriver.findElements(By.cssSelector("div.search-result-content div.thumb-link")).size();
        Assert.assertEquals(counter, expectedCount);
    }
}
