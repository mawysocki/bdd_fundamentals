package junit;

import io.cucumber.skeleton.Browsers;
import io.cucumber.skeleton.ShopPage;
import io.cucumber.skeleton.WebDriverFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AbstractTest {
    protected WebDriver webDriver;
    protected WebDriverWait wait;

    protected static final Browsers browser = Browsers.CHROME;

    @BeforeClass
    //Haczyk inicjuj�cy konfiguracj� chromedrivera
    public static void setup() {
        WebDriverFactory.setupSystemProperty(browser);
    }

    //Og�lny before dla ka�dej klasy testowej
    @Before
    public void openBrowser() {
        System.out.println("Abstract Before");
        //WebDriverFactory zwraca nam instancj� konkretnego drivera
        //w zale�no�ci jaka przegl�darka zosta�a ustawiona w te�cie
        webDriver = WebDriverFactory.getWebDriver(browser);
        //Otwieranie okna z maksymaln� rozdzielczo�ci�
        webDriver.manage().window().maximize();
        //Zakomentowane s� r�czne przypisania Webdriver�w
//        webDriver = new EdgeDriver();
//        webDriver = new FirefoxDriver();
//        webDriver = new ChromeDriver();
        wait = new WebDriverWait(webDriver, 5);
    }

//    @After - zakomentowane, aby nie wy��cza�o nam przegl�darki dla cel�w edukacyjnych
    public void CloseBrowser() {
        webDriver.quit();
    }


}
