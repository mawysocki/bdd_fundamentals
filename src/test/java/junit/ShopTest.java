package junit;

import io.cucumber.skeleton.Browsers;
import io.cucumber.skeleton.ShopPage;
import org.junit.Before;
import org.junit.Test;

//Testy sklepu z wykorzystaniem JUnit oraz Page Object Pattern
//Klasa testowa dziedziczy po AbstractTest w kt�rej ukryte s� haczyki oraz pola Webdrivera, WebDriverWaita jak i ShopPage
public class ShopTest extends AbstractTest{
    //W tym miejscu wybieramy w jakiej przegl�drce chcemy uruchomi� testy

    private ShopPage page;

    @Before
    public void init() {
        System.out.println("Shop before");
        //Before dedykowany dla danej klasy. Nie koliduje z before w klasie nadrz�dnej
        page  = new ShopPage(webDriver, wait);
        page.openShopWebsite();
    }
    @Test
    public void visitShopTest() {
        System.out.println("Visit shop");
        page.openShopWebsite();
    }

    @Test
    public void filterSsizeTest() {
        filterTest("S");
    }

    @Test
    public void filterXLsizeTest() {
        filterTest("XL");
    }

    //Dwa testy robi� dok�adnie to samo dlatego logika zosta�a wyodr�bniona do funkcji prywatnej
    //R�ni si� tylko danymi wej�ciowymi
    private void filterTest(String size) {
        page.selectSize(size);
        page.verifyShirtsOnPage(page.getExpectedCountForSize(size));
    }

    @Test
    public void selectItemTest() {
        int index = 4;
        double expectedTotalPrice = 58.90;
        page.selectItem(index);
        page.closeCart();
        page.selectItem(index);
        page.verifyTotalPrice(expectedTotalPrice);
    }

    @Test
    public void SelectShirtByNameTest() {
        String shirt1 = "Blue Sweatshirt";
        String shirt2 = "Black Tule Oversized";
        double expectedTotalPrice = 51.95;
        page.selectSize("S");
        page.selectItem(shirt1);
        page.selectSize("S");
        page.closeCart();
        page.selectSize("ML");
        page.selectItem(shirt2);
        page.verifyTotalPrice(expectedTotalPrice);
    }

    @Test
    public void SelectRandomShirtTest() {
        page.selectRandomItem();
        page.closeCart();
        page.selectRandomItem();
        page.verifyTotalPrice();
    }
}
