package toolsQA;

import junit.AbstractTest;
import org.junit.Before;
import org.junit.Test;
import toolsqa.AlertsPage;
import toolsqa.ElementsPage;
import toolsqa.ToolsQAHomePage;

public class MainPageTest extends AbstractTest {

    ToolsQAHomePage homePage;

    @Before
    public void init() {
        System.out.println("Shop before");
        //Before dedykowany dla danej klasy. Nie koliduje z before w klasie nadrzędnej
        homePage  = new ToolsQAHomePage(webDriver, wait);
        homePage.openPage();
    }
    @Test
    public void mainPageTest() {
        homePage.openPage();
    }

    @Test
    public void openElementsTest() {
        ElementsPage elementsPage = homePage.openElements();
        elementsPage.expand();
        elementsPage.expandTree();
        elementsPage.selectCheckBox();
        elementsPage.verifyText();
    }

    @Test
    public void openAlertsTest() {
        AlertsPage alertsPage = homePage.openAlerts();
//        alertsPage.expand();
        alertsPage.selectAlerts();
        alertsPage.clickTimeAlert();
        alertsPage.verifyAlert();
    }



}
