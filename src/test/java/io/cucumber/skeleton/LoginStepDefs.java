package io.cucumber.skeleton;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.skeleton.herokuapp.LoginPage;
import io.cucumber.skeleton.herokuapp.LoginVerificationPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

//Wykorzystuje Page Object Pattern
public class LoginStepDefs {

    private WebDriver webDriver;
    private WebDriverWait wait;
    private LoginPage loginPage;
    LoginVerificationPage verificationPage;

    //Zakomentowane poniewa� gryzie si� z haczykami z klasy ShopStepDefs.
    //Normalnie testy dw�ch r�nych miejsc nie powinny by� w tym samym projekcie
//    @Before // - zakomentowane aby nie gryz�o si� z innym @Before z klasy LoginStepDefs. Nie powinny istnie� razem w tym samym projekcie
    public void iOpenABrowser() {
        WebDriverFactory.setupSystemProperty(Browsers.CHROME);
        webDriver = WebDriverFactory.getWebDriver(Browsers.CHROME);
        wait = new WebDriverWait(webDriver, 5); //opoznienie w czasie
        System.out.println("Otwieram przegladarke");

        loginPage = new LoginPage(webDriver, wait);

        loginPage.goTo();
        System.out.println("Wchodze na strone");
    }

//    @After //Zakomentowane, aby nie wy��cza�o nam automatycznie przegl�darki do cel�w edukacyjnych
    public void closeConnection() {
        webDriver.quit();
        System.out.println("Zamykam");
    }

    @When("I enter login {string} and password {string}")
    public void iEnterLoginAndPassword(String login, String password) {
        loginPage.enterLogin(login);
        loginPage.enterPassword(password);
    }

    @And("I click enter button")
    public void iClickEnterButton() {
        loginPage.clickLoginButton();
        verificationPage = new LoginVerificationPage(webDriver, wait);

    }

    @Then("I see that the user is logged in")
    public void iSeeThatTheUserIsLoggedIn() {
        verificationPage.userShouldBeLoggedIn();
    }

    @Then("I see that the user is not logged in")
    public void iSeeThatTheUserIsNotLoggedIn() {
        verificationPage.userShouldNotBeLoggedIn();
    }
}