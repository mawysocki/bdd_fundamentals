package io.cucumber.skeleton;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class BideronkaSteps {
    WebDriver webDriver;
    WebDriverWait wait;
    public final String URL = "https://home.biedronka.pl/";
    @Given("I open browser")
    public void iOpenBrowser() {
        WebDriverFactory.setupSystemProperty(Browsers.CHROME);
        webDriver = WebDriverFactory.getWebDriver(Browsers.CHROME);
        wait = new WebDriverWait(webDriver, 5);
        webDriver.manage().window().maximize();
    }

//    @After
    public void closeBrowser() {
        webDriver.quit();
    }
    @And("I go Biedronka home")
    public void iGoBiedronkaHome() {
        webDriver.get(URL);
    }
    @Then("I see Biedronka logo")
    public void iSeeBiedronkaLogo() {
        wait.until(ExpectedConditions.urlToBe(URL));
        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("a img.header-logo__content")));
    }

    @When("I select {string} category")
    public void iSelectCategory(String category) {
        String css = "a." + category;
        webDriver.findElement(By.cssSelector(css)).click();
    }

    @Then("I see page with {string} category")
    public void iSeePageWithCategory(String category) {
        String css = String.format("div[data-category-id=\"%s\"]", category);
        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(css)));
    }

    @When("I expand price filter")
    public void iExpandPriceFilter() {
        webDriver.findElement(By.cssSelector("div.js-refinement-price svg")).click();

    }

    @And("I select the cheapest filter")
    public void iSelectTheCheapestFilter(){
        String css = "li.refinement-price__list a";
        List<WebElement> elements = webDriver.findElements(By.cssSelector("li.refinement-price__list a"));
        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(css)));
        elements.get(0).click();
    }

    @And("I accept cookies")
    public void iAcceptCookies() {
        String css = "button#onetrust-accept-btn-handler";
        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(css)));
        webDriver.findElement(By.cssSelector(css)).click();
    }

    @Then("Verify if items are in range {int} to {int}")
    public void verifyIfItemsAreInRangeTo(int min, int max) {
        String css = "div.refinement-pill__item";
        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(css)));
        List<WebElement> elements = webDriver.findElements(By.cssSelector("div.price-tile__sales "));
        System.out.println(elements.size());
        int x = 1;
        for (WebElement element : elements) {
            String text = element.getText();
            text = text.replace(".",""); //Kropa jest separatorem dla liczb powy�ej 1k i trzeba j� wywali�
            String[] s = text.split("\n"); //Rozdzielamy na entery poniewa� pobiera ca�� cen� w��cznie z groszami i walut�
            System.out.println(x + ": " + s[0]); //Interesuje nas tylko pierwsza cz��, czyli cena ca�kowita
            int price = Integer.parseInt(s[0]); //Aby m�c por�wnywa� liczby, nale�y
            Assert.assertTrue(price <= max - 1);
            Assert.assertTrue(price > min);
            x++;
        }
    }

    @And("I select filter {int}")
    public void iSelectFilter(int index) {
        String css = "li.refinement-price__list a";
        List<WebElement> elements = webDriver.findElements(By.cssSelector("li.refinement-price__list a"));
        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(css)));
        elements.get(index-1).click();
    }

    @When("I add to cart {int} element")
    public void iAddToCartElement(int index) {
        webDriver.findElements(By.cssSelector("div.product-tile")).get(index-1).click();
        webDriver.findElement(By.cssSelector("button.add-to-cart")).click();
    }
}
