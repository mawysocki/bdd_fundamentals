package io.cucumber.skeleton;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.Random;

public class ShopStepsDef {

    //Deklaracja kilku podstawowych zmiennych wykorzystywanych w naszych testach
    //Webdriver to g��wne narz�dzie do manipulowania przegl�dark�
    WebDriver webDriver;
    //Webdriverwait posiada w sobie mechanizmy do inteligentnego czekania na elementy
    WebDriverWait wait;

    //Adress URL dobrze przechowywa� jako wyodr�bniona sta�a wpisywa� bezpo�rednio w te�cie
    final String url = "https://react-shopping-cart-67954.firebaseapp.com/";
    //Wyodr�bnione selektory, kt�re wykorzystywane s� w kliku miejscach. Dlatego nie mog� le�e� w pojedynczej metodzie
    final String addToCartButton = "button.sc-124al1g-0";
    final String itemSelector = "main div.sc-124al1g-2";

    double currentTotalPrice = 0;

    //Metoda - haczyk z biblioteki io.cucumber.java
    //@Before oznacza metod� aby uruchamia�a si� automatycznie przed ka�dym testem
    //Pozosta�e oznaczenia: @BeforeStep @BeforeAll
    //UWAGA! BeforeAll wyst�puje r�wniez w pakiecie org.junit.jupiter.api, wi�c przy imporcie nale�y sprawdzi� jego poprawno��
    //B��dny import nie wy�wietli b��du w InteliJ ale b�dzie nieporawid�owo dzia�a�
//   @Before //zakomentowane aby nie gryz�o si� z innym @Before z klasy LoginStepDefs. Nie powinny istnie� razem w tym samym projekcie
    public void iOpenABrowser() {
        //Ustawia zmienn� systemow� aby znajdowa�o nam ChromeDrivera
       WebDriverFactory.setupSystemProperty(Browsers.CHROME);
//        System.setProperty("webdriver.chrome.driver", "C:\\Webdrivers\\chromedriver.exe");
        //Tworzenie instancji webdriver otwiera nam r�wnie� fizycznie przegl�dark� na komputerze
        //Poza tym bez zainicjalizowania zmiennej, nie mogliby�my jej u�y�
        //M�g�by pojawia� si� wyj�tek "NullPointerException"
        webDriver = WebDriverFactory.getWebDriver(Browsers.CHROME);
//        webDriver = new ChromeDriver();
        //Inicjalizacja zmiennej typu WebDriverWait. W konstruktorze nale�y przekaza� 2 parametry
        //1 - Sam webdriver stworzony powy�ej, 2 - domy�lny czas czekania na element w sekundach
        wait = new WebDriverWait(webDriver, 5);
        //Maksymalizuje okno przegl�darki
        webDriver.manage().window().maximize();
    }

    //    @After //Zakomentowane, aby nie wy��cza�o nam automatycznie przegl�darki do cel�w edukacyjnych
    public void closeTest() {
        webDriver.quit();
    }

    @When("I go to shop website")
    public void iGoToShopWebsite() {
        //Wej�cie na stron� za pomoc� metody get i u�ycia sta�ej jako parametru
        webDriver.get(url);
        //Wywo�anie kroku do weryfikacji wraz z wej�ciem na stronie powoduje,
        //�e sprawdzany jest stan koszulek automatycznie po wej�ciu na stron� bez potrzeby wywo�ywania kroku
        iSeeTShirtsInOffer(16);
    }

    @Then("I see {int} t-shirts in offer")
    public void iSeeTShirtsInOffer(int count) {
        //Czekanie na elementy poprzez zamro�enie programu na x milisekund
        //NIEZALECANE !!!
//        try {
//            Thread.sleep(2000);
//        } catch (InterruptedException e) {
//            throw new RuntimeException(e);
//        }
        //Selektor mo�e ulec zmianie

        //Inteligentne czekanie na elementy
        wait.until(ExpectedConditions.numberOfElementsToBe(By.cssSelector(itemSelector), count));
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector(itemSelector)));
        int size = getTotalCount();
        System.out.println("Widz� " + size + " koszulek");
        System.out.println("Oczekuj� " + count + " koszulek");

        //Trzy rodzaje sprawdzenie tej samej warto�ci poprzez:
        // 1) Sprawdzenie r�wno�ci aktualnego rozmiaru i oczekiwanego
        // 2) Sprawdzenie czy jest prawd�, �e aktualny rozmiar jest r�wny oczekiwanemu
        // 3) Sprawdzenie czy NIE jest prawd�, �e aktualny pomiar NIE jest r�wny oczekiwanemu
        Assert.assertEquals(count, size);
        Assert.assertTrue(count == size);
        Assert.assertFalse(count != size);
    }

    @When("I select {string} size")
    public void iSelectSize(String size) {
        //Aby wej�� w interakcj� z jakim� elementem nale�y go najpierw zlokalizowa�
        //Nale�y to zrobi� za pomoc� jakiego� rodzaju selektora jak CSS czy Xpaht
        //Dobrze taki selektor przypisa� wcze�niej do zmiennej, aby u�ycie go by�o prostsze, a kod czytelniejszy
        String xpath = String.format("//span[text() = '%s']", size);

        //U�ycie zmiennej zwi�ksza czytelno�� kodu. Kilka dodatkowych nawias�w z xPatha mog�oby tu nabru�dzi�
        WebElement element = webDriver.findElement(By.xpath(xpath));
        element.click();
        System.out.println("Wybieram rozmiar: " + size);
    }

    @When("I click {int} Item")
    public void iClickItem(int index) {
        //Wybranie elementu za pomoc� indeksu
        //Uwaga index-1 poniewaz indeksujemy od zera, wi�c pierwszy element b�dzie tak naprawd� drugim itd
        webDriver.findElements(By.cssSelector(addToCartButton)).get(index - 1).click();

    }

    @And("I select {string} t-shirt")
    public void iSelectTShirt(String name) {
        //Za pomoc� Xpatha mo�emy znale�� element z nazw� koszulki
        //po czym przej�� do g�ry za pomoc� dw�ch kropek,
        //aby zej�� na d� do przycisku "add to cart" dla tego samego elememtu
        String selector = String.format("//p[text() = '%s']//..//button", name);
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(selector)));
        webDriver.findElement(By.xpath(selector)).click();
    }

    @When("I select random t-shirt")
    public void iSelectRandomTShirt() {
        //Obiekt klasy odpowiedzialny za losowanie
        Random r = new Random();
        //Sprawdzenie ilo�ci wszystkich koszulek, aby ograniczy� tym zakres koszulek
        int index = r.nextInt(getTotalCount());
        //Zebranie wszystkich kontener�w przechowuj�cych koszulki (div)
        List<WebElement> elements = webDriver.findElements(By.cssSelector(itemSelector));
        //Pobranie ceny z wylosowanej koszulki
        WebElement container = elements.get(index);
        String price = container.findElement(By.cssSelector("p.sc-124al1g-6")).getText();
        //Konwersja ceny na typ liczbowy - oddzielna metoda
        double realPrice = extractPrice(price);
        //Dodanie ceny obecnej koszulki do ca�kowitej
        currentTotalPrice += realPrice;
        //Klikni�cie w "Add to cart" dla wylosowanej koszulki, aby doda� j� do koszyka
        container.findElement(By.cssSelector("button")).click();
        System.out.println("Price: " + currentTotalPrice);
    }

    @And("I close cart")
    public void iCloseCart() throws InterruptedException {
        By selector = By.cssSelector("button.sc-1h98xa9-0");
        //Inteligentne czekanie na element a� b�dzie nadawa� si� do klikni�cia
        wait.until(ExpectedConditions.elementToBeClickable(selector));
        webDriver.findElement(selector).click();
        //Czekanie a� koszyk si� zamknie i pojawi si� element element z danym atrybutem (ikona koszyka do otwarcia go)
        //UWAGA! potrafi si� przyci�� przy wi�kszej liczbie test�w puszczonej na raz.
        wait.until(ExpectedConditions.attributeToBe(By.cssSelector("div.sc-1h98xa9-3"), "title", "Products in cart quantity"));
    }

    @Then("Verify if total price is {double}")
    public void verifyIfTotalPriceIs(double expectedPrice) {
        //NIEZALECANE !!!
//        try {
//            Thread.sleep(1000);
//        } catch (InterruptedException e) {
//            throw new RuntimeException(e);
//        }
        String selector = "p.sc-1h98xa9-9";
        //Czekanie a� koszyk b�dzie otwarty
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector(selector)));
        //Pobranie ceny w postaci tekstu
        String price = webDriver.findElement(By.cssSelector(selector)).getText();
        //Zamiana na typ liczbowy, aby mo�na by�o sprawdzi� poprawno��
        double realPrice = extractPrice(price, true);
        //Weryfikacja ceny na dwa sposoby
        System.out.println(realPrice);
        System.out.println(expectedPrice);
        Assert.assertTrue(realPrice == expectedPrice);
        //Przy por�wnywaniu typu double nale�y u�y� equal tr�jargumentowego z delt�
        Assert.assertEquals(realPrice, expectedPrice, 0.0);
    }


    @Then("Verify total price")
    public void verifyTotalPrice() {
        //Weryfikacja ceny ca�kowitej bez wpisywania z palca.
        //Cena musi by� zliczana w innych krokach
        verifyIfTotalPriceIs(currentTotalPrice);
    }

    //Pomocnicza metoda do sprawdzania liczby koszulek
    //Odseparowana, poniewa� wyst�puj� w kilku miejscach
    private int getTotalCount() {
        return webDriver.findElements(By.cssSelector(itemSelector)).size();
    }


    //Metoda do wyelminowania dodatkowych znak�w z pobieranej ceny
    //Konwertuje od razu String do typu Double
    //Argument isSpace jest wymagany gdy� cena za koszulk� ma tylko dodatkowy znak: "$cena"
    //Natomiast cena ca�kowita ma jeszcze spacj�: "$ cena"
    private double extractPrice(String price, boolean isSpace) {
        StringBuilder unwantedMark = new StringBuilder("$");
        if (isSpace) {
            unwantedMark.append(" ");
        }
        price = price.replace(unwantedMark.toString(), "");
        return Double.parseDouble(price);
    }

    //Przeci��enie metody extractPrice
    //Je�li chcemy wydoby� cen� bez spacji to nie musimy podawa� w argumencie isSpace=false.
    //U�ycie jednoargumentowej metody spowoduje wywo�anie jej z warto�ci� isSpace=false
    private double extractPrice(String price) {
        return extractPrice(price, false);
    }
}
