package herokuapp;

import io.cucumber.skeleton.Browsers;
import io.cucumber.skeleton.WebDriverFactory;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HerokuAppAbstract {

    public final String PROTOCOL = "https://";
    public final String URL = "the-internet.herokuapp.com/";
    public final String FULL_ADRESS = PROTOCOL + URL;
    protected WebDriver webDriver;
    protected WebDriverWait wait;
    @Before
    public void openBrowser() {
        WebDriverFactory.setupSystemProperty(Browsers.CHROME);
        webDriver = WebDriverFactory.getWebDriver(Browsers.CHROME);
        wait = new WebDriverWait(webDriver, 5);
        webDriver.manage().window().maximize();
        goToMainPage();
    }

    private void goToMainPage() {
        webDriver.get(FULL_ADRESS);
    }

//    @After
    public void closeBrowser() {
        webDriver.close();
        webDriver.quit();
    }
}
