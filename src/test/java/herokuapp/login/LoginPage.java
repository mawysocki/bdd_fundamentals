package herokuapp.login;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage {
    WebDriver webDriver;

    public LoginPage(WebDriver webDriver) {
        this.webDriver = webDriver;
    }
    public  void typeInForm(InputType type, String value) {
        webDriver.findElement(By.id(type.ID)).sendKeys(value);
    }
    public void clickButton() {
        webDriver.findElement(By.cssSelector("button[type='submit']")).click();
    }
}
