package herokuapp.login;

enum InputType {
    LOGIN("username"), PASSWORD("password");

    public final String ID;
    InputType(String id) {
        ID = id;
    }
}