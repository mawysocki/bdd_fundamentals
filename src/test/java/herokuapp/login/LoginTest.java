package herokuapp.login;

import herokuapp.HerokuAppAbstract;
import org.junit.Before;
import org.junit.Test;

public class LoginTest extends HerokuAppAbstract {

    final String correctLogin = "tomsmith";
    final String inCorrectLogin = "incorrectLogin";
    final String correctPassword = "SuperSecretPassword!";
    final String inCorrectPassword = "IncorrectPassword!";

    private LoginPage loginPage;
    private VerificationPage verificationPage;
    @Before
    public void goToLoginPage() {
        webDriver.get(FULL_ADRESS + "login");
        loginPage = new LoginPage(webDriver);
        verificationPage = new VerificationPage(webDriver, wait);
    }

    @Test
    public void useCorrectLoginAndPasswordTest() {
        loginPage.typeInForm(InputType.LOGIN, correctLogin);
        loginPage.typeInForm(InputType.PASSWORD, correctPassword);
        loginPage.clickButton();
        verificationPage.userShouldBeLoggedIn();
    }

    @Test
    public void useIncorrectPasswordTest() {
        loginPage.typeInForm(InputType.LOGIN, correctLogin);
        loginPage.typeInForm(InputType.PASSWORD, inCorrectPassword);
        loginPage.clickButton();
        verificationPage.userHasIncorrectPassword();
    }

    @Test
    public void useIncorrectLoginTest() {
        loginPage.typeInForm(InputType.LOGIN, inCorrectLogin);
        loginPage.typeInForm(InputType.PASSWORD, correctPassword);
        loginPage.clickButton();
        verificationPage.userHasIncorrectLogin();
    }



}

