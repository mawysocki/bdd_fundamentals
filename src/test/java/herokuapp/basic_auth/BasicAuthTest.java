package herokuapp.basic_auth;

import herokuapp.HerokuAppAbstract;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;


public class BasicAuthTest extends HerokuAppAbstract {

    private final String USERNAME = "admin", PASSWORD = "admin", ENDPOINT = "basic_auth";

    @Test
    public void loginByAuthTest() throws InterruptedException {
        webDriver.get(getUrl());
        System.out.println(FULL_ADRESS + ENDPOINT);
        String expectedText = "Congratulations! You must have the proper credentials.";
        String textOnPage = webDriver.findElement(By.tagName("p")).getText();
        Assert.assertEquals(expectedText, textOnPage);
    }

    private String getUrl() {
        StringBuilder url = new StringBuilder(PROTOCOL);
        url.append(USERNAME)
                .append(":")
                .append(PASSWORD)
                .append("@")
                .append(URL)
                .append(ENDPOINT);
        return url.toString();
    }
}
